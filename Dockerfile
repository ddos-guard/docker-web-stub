FROM nginx:alpine

COPY index.html /user/share/html/index.html

COPY default.conf /etc/nginx/conf.d/default.conf